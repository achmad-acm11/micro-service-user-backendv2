# Base image for building
FROM amd64/node:17.4.0-alpine3.15 AS build
# FROM amd64/node:17.4.0-slim AS build

# Change working directory
WORKDIR /app

# Copy package manifest
COPY ["package.json", "package-lock.json*", "./"]

# Install all dependencies
RUN npm install

# Copy source code
COPY . .

# Bootstrap app
CMD ["node", "./bin/www"]

